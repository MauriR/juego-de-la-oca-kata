const START = 0

class Players {
  constructor() {
    this.list = []
  }
  
  add(name) {
    const capitalizedName = this.capitalizeName(name)
    const player = new Player(capitalizedName)
    this.list.push(player.name)
    this.list = Array.from(new Set(this.list))
    return player
  }

  capitalizeName(name) {
    return name[0].toUpperCase() + name.toLowerCase().slice(1)
  }
}

class Player {//innecesario?
  constructor(name) {
    this.name = name
    this.position = START
  }
}

module.exports = Players