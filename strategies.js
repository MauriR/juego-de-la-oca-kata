class StrategyWin {
  constructor(player ,notInUse,neitherInUse) { 
    this.player = player
  }
  matchesCondition() {
    return this.player.position === 63
  }
  applyMovement() {
   
  }
  reportMovement(){
    return `${this.player.name} wins the Game!`
  }
}

class StrategyBounce {
  constructor(player ,dice, initialPosition) { 
    this.player = player
    this.dice = dice
    this.initial = initialPosition
  }
  matchesCondition() {
    return this.player.position > 63
  }

  applyMovement() {
    this.player.position = 63 - (this.player.position - 63)
  }
  reportMovement(){
    return `${this.player.name} rolls ${this.dice[0]} and ${this.dice[1]}. ${this.player.name} moves from square ${this.initial} to square 63. ${this.player.name} bounces and returns to square ${this.player.position}`
  }
}

class StrategyBridge {
  constructor(player ,notInUse,neitherInUse) { 
    this.player = player
  }
  matchesCondition() {
    return this.player.position === 6
  }

  applyMovement() {
    this.player.position = 12
  }

  reportMovement(){
    return 
  }
}

class StrategySkull { 
  constructor(player,notInUse,neitherInUse) {
    this.player = player
  }
  matchesCondition() {
    return this.player.position === 58
  }

  applyMovement() {
    this.player.position = 0
  }

  reportMovement(){
    return 
  }
}

class StrategyInitialNine {
  constructor(player,dice, initial) {
    this.player = player
    this.dice = dice
    this.initial= initial
  }
  matchesCondition() {
    if (this.initial === 0) {
      return this.player.position === 9
    }
  }

  applyMovement() {
    this.player.position = Number(this.dice.join(''))
  }

  reportMovement(){
    return 
}}

const strategies = [
  StrategyInitialNine, StrategySkull, StrategyWin, StrategyBounce, StrategyBridge
]
module.exports = strategies