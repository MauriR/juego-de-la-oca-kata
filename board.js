const Player = require('./players')
const strategies = require('./strategies')
class Board {//board debe ocuparse de sus asuntos y dejar a las otras clases
  constructor() {
    this.player1 = {}
    this.player2 = {}
  }

  move(player, dice) {
    let initialPosition = player.position
    let movement = player.position += dice[0] + dice[1]
    for (let strategyClass of strategies) {
      const strategy = new strategyClass(player,dice,initialPosition)
      if (strategy.matchesCondition()) {
        strategy.applyMovement()
        movement = strategy.reportMovement()
      }
    }
    return movement
  }
}


module.exports = Board