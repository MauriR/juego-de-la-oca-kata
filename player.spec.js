const Players = require('./players')
describe('players', () => {
  it('can add players', () => {
    const players = new Players()

    players.add('Juan')

    expect(players.list).toEqual(['Juan'])
  })

  it('can add further players', () => {
    const players = new Players()

    players.add('Juan')
    players.add('Sara')

    expect(players.list).toEqual(['Juan', 'Sara'])
  })

  it('does not accept duplicated names',()=>{
    const players = new Players()

    players.add('JuAn')
    players.add('SARA')
    players.add('juan')

    expect(players.list).toEqual(['Juan', 'Sara'])
  })
  
})