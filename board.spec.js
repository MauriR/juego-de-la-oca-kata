const Board = require('./board')
describe('board', () => {
  it('can move a player based on dice score', () => {
    const board = new Board()
    board.player1.position = 0
    board.player2.position = 5
    const roll = [2, 3]

    board.move(board.player1, roll)
    board.move(board.player2, roll)

    expect(board.player1.position).toBe(5)
    expect(board.player2.position).toBe(10)
  })

  it('declares the first player reaching square 63 as the winner', () => {
    const board = new Board()
    board.player1.position = 58
    const roll = [2, 3]

    const winCondition = board.move(board.player1, roll)

    expect(board.player1.position).toBe(63)
    // expect(winCondition).toBe("Juan wins the Game!")
  })

  it('demands a player to win with an exact 63. Otherwise they Bounce back', () => {
    const board = new Board()
    board.player1.position = 60
    const roll = [2, 3]
    const bounceReport = 'Juan rolls 2 and 3. Juan moves from square 60 to square 63. Juan bounces and returns to square 61'

    const bounceCondition = board.move(board.player1, roll)

    expect(board.player1.position).toBe(61)
    // expect(bounceCondition).toBe(bounceReport)
  })

    it('applies the Bridge rule: If a player reaches with exact result the square 6, moves to square 12', () => {
      const board = new Board()
      board.player1.position = 0
      const roll = [2, 4]

      board.move(board.player1, roll)

      expect(board.player1.position).toBe(12)
    })

    it('applies the Skull rule: If a player reaches with exact result the square 58, moves to start', () => {
      const board = new Board()
      board.player1.position = 52
      const roll = [2, 4]

      board.move(board.player1, roll)

      expect(board.player1.position).toBe(0)
    })

    it('applies the "9" rule: In your first roll, if you score 9, you move to the square where the first dice is the ten and the second the unit', () => {
      const board = new Board()
      board.player1.position = 0
      const roll = [5, 4]

      board.move(board.player1, roll)

      expect(board.player1.position).toBe(54)
    })
  })